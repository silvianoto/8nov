<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;
/**
 * Description of ProductoModel
 *
 * @author a0x4537658b
 */
class ProductoModel extends Model{
    protected $table = 'productos';
    protected $primarykey = 'id';
    protected $returnType = 'object'; //porque me gusta
    //hemos de decir que campos son modificables
    protected $allowedFields = ['codigoproducto','nombre','codigofamilia','caracteristicas','color','tipoiva'];
}


