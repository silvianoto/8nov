<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of GrupoModel
 *
 * @author a0x4537658b
 */
namespace App\Models;
use CodeIgniter\Model;

class Productos extends Model {
    /*definimos parámetros de la tabla a la que queramos acceder*/
    protected $table='productos';
    protected $primaryKey= 'id';
    protected $returnType= 'object';
}
