<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

?>

<!-- Formulario para insertar datos -->
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <div class="container">
        <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('productos/alta')?>" method="post">
            <div class='form-product'>
                <label for="codigoproducto">Codigo Producto</label>
                <input type="text" name="codigo_producto" value="" id="codigo_producto" class="form-control"/>
            </div>
            
            <div class='form-product'>
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" value="" id="nombre" class="form-control"/>
            </div>
             
             <div class='form-product'>
                <label for="codigofamilia">Codigo Familia</label>
                <input type="text" name="codigo_familia" value="" id="codigo_familia" class="form-control"/>
            </div>
            
             <div class='form-product'>
                <label for="caracteristicas">Caracteristicas</label>
                <input type="text" name="caracteristicas" value="" id="caracteristicas" class="form-control"/>
            </div>
            
            <div class='form-product'>
                <label for="color">Color</label>
                <input type="text" name="color" value="" id="color" class="form-control"/>
            </div>
            
            <div class='form-product'>
                <label for="tipoiva">TipoIVA</label>
                <input type="text" name="tipoiva" value="" id="tipoiva" class="form-control"/>
            </div>
            <input type="submit" name="enviar" value="Enviar" />
            
        </form>
    </body>
</html> 



