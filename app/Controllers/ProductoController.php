<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ProductoModel; //decimos dónde está

/**
 * Description of AlumnoController
 *
 * @author a0x4537658b
 */
class ProductoController extends BaseController {
    
    public function formInsertProductos(){
        $data['title'] = 'Formulario Alta Producto';
        return view('productos/formalta', $data);
    }
    
   
    //este método será llamado tras rellenar los campos del formulario y enviarlo
    public function insertProduct() {
       //Tomar los datos del formulario
        $codigo_producto = $this->request->getPost('codigo');//es el valor del atributo name del campo input
        $nombre = $this->request->getPost('nombre');
        $codigo_familia = $this->request->getPost('codigo_familia');
        $caracteristicas = $this->request->getPost('caracteristicas');
        $color = $this->request->getPost('color');
        $tipoiva = $this->request->getPost('tipoiva');
        //lo pasamos a un array asociativo
        $producto_nuevo = [
            'codigo_producto' => $codigo_producto,
            'nombre' => $nombre,
            'codigo_familia' => $codigo_familia,
            'caracteristicas' => $caracteristicas,
            'color' => $color,
            'tipoiva' => $tipoiva,
        ];
        /*//comprobamos lo que hemos recibido del formulario
        echo '<pre>';
        print_r($producto_nuevo);
        echo '</pre>';*/
        //Si trabajamos con la base de datos necesitamos un modelo
        //creamos el modelo que nos interesa
        $ProductoModel = new AlumnoModel();
        //usamso el métedo insert del modelo para añadir los datos de un alumno
        $ProductoModel->insert($producto_nuevo);
        //Ahora queremos que siga añadiendo nuevos alumnos
        return redirect()->to('productos/formalta');
        
    /*}*/
   

    }
}